snippets = (
    # 'fix_utils',
    'helper',
    'messages',
    'karma',
    'bounty',
)


def setup():
    for item in snippets:
        module = getattr(__import__(__package__ + '.' + item), item)
        if hasattr(module, '__title__'):
            title = getattr(module, '__title__')
        else:
            title = module.__name__
        print('Loaded:', title)
        if hasattr(module, 'setup'):
            module.setup()
