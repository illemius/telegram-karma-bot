from datetime import datetime, timedelta

from meta import bot, bot_id, TIMEZONE
from models.messages import Messages
from utils.chat import crash_message, get_username_or_name, typing, check_dialog

__title__ = 'Messages'


@bot.message_handler(commands=['messages'])
def cmd_messages_count(message):
    def callback(msg, chat_id):
        typing(msg)
        date = datetime.now(TIMEZONE) - timedelta(days=1)
        users = Messages.calculate(chat_id, date)

        text = ['Топ флудеров за последние 24 часа:']

        if bot_id in users:
            users.pop(bot_id)

        for index, (user, messages) in enumerate(sorted(users.items(), key=lambda x: x[1], reverse=True), start=1):
            if index > 10:
                break
            text.append('{}) {}: {} ✉ (сбщ.)'.format(index, get_username_or_name(bot.get_chat(user)), messages))
        bot.send_message(msg.chat.id, '\n'.join(text), disable_notification=True)

    check_dialog(message, callback, Messages)
