from config import ROOT_UID
from meta import bot
from models.karma import Karma
from models.messages import Messages
from utils.chat import crash_message
from utils.karma import log


@bot.message_handler(commands=['fix_messages'])
def cmd_fix_messages(message):
    """
    Add field 'is_command' to Messages document.
    :param message:
    :return:
    """
    try:
        if not message.from_user.id == ROOT_UID:
            return

        messages = Messages.objects.all()
        messages.update(set__is_command=False)
        log.info('Fixed {} messages.'.format(len(messages)))
        bot.reply_to(message, 'Fixed {} messages.'.format(len(messages)))
    except:
        crash_message(message)


@bot.message_handler(commands=['fix_messages_2'])
def cmd_fix_messages(message):
    """
    Add field 'is_command' to Messages document.
    :param message:
    :return:
    """
    try:
        if not message.from_user.id == ROOT_UID:
            return

        messages = Messages.objects.all()
        for msg in messages:
            msg.chat_id = msg.chat
            # message.chat = None
            msg.user_id = msg.user
            # message.user_id = None
            msg.save()
        log.info('Fixed {} messages.'.format(len(messages)))
        bot.reply_to(message, 'Fixed {} messages.'.format(len(messages)))
    except:
        crash_message(message)


@bot.message_handler(commands=['fix_messages_3'])
def cmd_fix_messages(message):
    """
    Add field 'is_command' to Messages document.
    :param message:
    :return:
    """
    try:
        if not message.from_user.id == ROOT_UID:
            return

        messages = Messages.objects.all()
        for message in messages:
            message.chat = None
            message.user = None
            message.save()
        log.info('Fixed {} messages.'.format(len(messages)))
        bot.reply_to(message, 'Fixed {} messages.'.format(len(messages)))
    except:
        crash_message(message)


@bot.message_handler(commands=['fix_karma'])
def cmd_fix_carma(message):
    try:
        if not message.from_user.id == ROOT_UID:
            return

        karma_objects = Karma.objects.all()
        for karma in karma_objects:
            karma.chat_id = karma.chat
            karma.save()
        log.info('Fixed {} records.'.format(len(karma_objects)))
        bot.reply_to(message, 'Fixed {} records.'.format(len(karma_objects)))
    except:
        crash_message(message)


@bot.message_handler(commands=['fix_karma_2'])
def cmd_fix_carma(message):
    try:
        if not message.from_user.id == ROOT_UID:
            return

        karma_objects = Karma.objects.all()
        for karma in karma_objects:
            karma.chat = None
            karma.save()
        log.info('Fixed {} records.'.format(len(karma_objects)))
        bot.reply_to(message, 'Fixed {} records.'.format(len(karma_objects)))
    except:
        crash_message(message)
