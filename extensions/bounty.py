from datetime import datetime, timedelta

import schedule

import telebot
from config import LOGGING_CHAT, BONUS_PLUS, ROOT_UID
from meta import bot, TIMEZONE
from models.bounty import Bounty
from models.karma import Karma
from models.messages import Messages
from utils.chat import crash_message, check_dialog
from utils.chat_logger import get_chat_logger

__title__ = 'Bounty'

log = get_chat_logger(LOGGING_CHAT, 'bounty')

COEFFICIENT_1 = .00029
COEFFICIENT_2 = .4
COEFFICIENT_DIV = 4


def setup():
    schedule.every().hour.do(task)


def task(fake=False):
    date = datetime.now(TIMEZONE) - timedelta(hours=6)
    temp = {}
    for message in Messages.objects(creation_date__gt=date, is_command=False):
        # unpack
        user_obj = temp.get(message.user_id, {})
        chat_obj = user_obj.get(message.chat_id, {})
        # update
        chat_obj.update({'messages': chat_obj.get('messages', 0) + 1})
        if 'karma' not in chat_obj:
            chat_obj['karma'] = Karma.get_user_chat_karma(message.chat_id, message.user_id)
        # set new
        user_obj[message.chat_id] = chat_obj
        temp[message.user_id] = user_obj

    payments_log = []
    bounty_list = []
    chats_list = []
    users_list = []
    for user_id, user in temp.items():
        if user_id not in users_list:
            users_list.append(user_id)
        for chat_id, chat in user.items():
            if chat_id not in chats_list:
                chats_list.append(chat_id)
            if (chat.get('karma') or 0) <= 0 or (chat.get('messages') or 0) <= 0:
                continue
            amount = calculate_payment(chat.get('messages'), chat.get('karma'), BONUS_PLUS)
            bounty_list.append(amount)
            if not fake:
                Bounty.transaction(chat_id=chat_id, to_user=user_id, amount=amount, description='salary')
            payments_log.append(':: User {} in the chat {} received {} (K:{}, M:{})'.format(
                user_id, chat_id, amount, chat.get('karma'), chat.get('messages')))
    msg = '{users} user(s) got {sum} bounty in {chats} chat(s)\nMin: {min} - Max: {max}'.format(
        sum=sum(bounty_list), users=len(users_list), chats=len(chats_list), min=min(bounty_list), max=max(bounty_list)
    ) + '\n' + '\n'.join(payments_log)

    if fake:
        msg = '#fake ' + msg
    if len(payments_log) > 0:
        log.info(msg)
    return msg


def calculate_payment(messages, karma, bonus):
    if karma <= 0 or messages <= 0:
        return .0
    amount = ((messages * (karma * COEFFICIENT_1)) ** COEFFICIENT_2) / COEFFICIENT_DIV
    return round(abs(amount + amount * (abs(bonus) / 100)), 2)


@bot.message_handler(commands=['test_bounty'])
def cmd_test_bounty(message):
    try:
        text = message.text.split()
        messages = 0
        karma = 0
        bonus = 0
        try:
            messages = int(text[1])
        except:
            pass
        try:
            karma = int(text[2])
        except:
            pass
        try:
            bonus = abs(float(text[3]))
        except:
            pass
        result = calculate_payment(messages, karma, bonus)
        bot.reply_to(message, '\n'.join([
            'Messages: ' + str(messages),
            'Karma: ' + str(karma),
            'Bonus: +' + str(bonus) + '%',
            '💸 per hour: ' + str(result),
            'Total 💸 per day: ' + str(round(result * 24, 2))
        ]))
    except:
        crash_message(message)


@bot.message_handler(commands=['my_salary'])
def cmd_get_my_salary(message):
    def callback(msg, chat_id):
        date = datetime.now(TIMEZONE) - timedelta(hours=6)
        messages = len(Messages.objects(creation_date__gt=date, is_command=False,
                                        chat_id=chat_id, user_id=message.from_user.id))
        karma = Karma.get_user_chat_karma(chat_id, message.from_user.id)
        salary = calculate_payment(messages, karma, BONUS_PLUS)
        bot.reply_to(message, '\n'.join([
            'Сообщения: ' + str(messages) + ' ✉',
            'Карма: ' + str(karma) + ' 🍪',
            'Бонус: +' + str(BONUS_PLUS) + '% 💰',
            'Прибыль в час: ' + str(salary) + ' 💸',
            'Прибыль в день: ~' + str(round(salary * 24, 2)) + ' 💸',
            'В кармане: ' + str(round(Bounty.get_user_bounty(chat_id, msg.from_user.id), 2)) + ' 💸'
        ]))

    check_dialog(message, callback, Bounty)


@bot.message_handler(commands=['wallet'])
def cmd_wallet(message):
    def callback(msg: telebot.types.Message, chat_id):
        try:
            amount = Bounty.get_user_bounty(chat_id, message.from_user.id)
            return bot.reply_to(message, 'Ваш счет: {:.2f} 💸'.format(amount))
        except:
            crash_message(message)

    check_dialog(message, callback, Bounty)


@bot.message_handler(commands=['salary_task'])
def cmd_qwe(message):
    if message.from_user.id == ROOT_UID:
        if 'fake' in message.text:
            result = task(fake=True)
        else:
            result = task()
        bot.reply_to(message, result)
