import datetime

from mongoengine import *

from config import INIT_BONUS
from meta import TIMEZONE


class Bounty(Document):
    chat_id = IntField()
    from_user = IntField(default=0)
    to_user = IntField(default=0)

    amount = FloatField(default=0.0)
    transfer = BooleanField()
    description = StringField(default='transaction')

    rollback = BooleanField(default=False)

    # Logging
    creation_date = DateTimeField(default=datetime.datetime.now)
    modified_date = DateTimeField(default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        if not self.creation_date:
            self.creation_date = datetime.datetime.now(TIMEZONE)
        self.modified_date = datetime.datetime.now(TIMEZONE)
        return super(Bounty, self).save(*args, **kwargs)

    def revoke(self):
        self.rollback = True
        self.save()

    def cancel(self):
        self.rollback = not self.rollback
        self.save()

    @classmethod
    def get_transaction(cls, transaction):
        transactions = Bounty.objects(id=transaction)
        if len(transactions) > 0:
            return transactions[0]
        return None

    @classmethod
    def get_chat(cls, chat_id):
        return Bounty.objects(chat_id=chat_id)

    @classmethod
    def get_chat_karma(cls, chat_id):
        records = Bounty.get_chat(chat_id)
        result = {}
        for bounty in records:
            if bounty.rollback:
                continue
            if bounty.from_user and bounty.transfer:
                result[bounty.from_user] = result.get(bounty.from_user, 0) - bounty.amount
            if bounty.to_user:
                result[bounty.to_user] = result.get(bounty.to_user, 0) + bounty.amount
        return result

    @classmethod
    def get_user_bounty_transactions(cls, chat_id, user_id):
        return Bounty.get_chat(chat_id).filter(Q(to_user=user_id) | Q(from_user=user_id), rollback=False)

    @classmethod
    def get_user_bounty(cls, chat_id, user_id):
        result = 0
        for bounty in cls.get_user_bounty_transactions(chat_id, user_id):
            if bounty.from_user and bounty.transfer:
                result -= bounty.amount
            if bounty.to_user:
                result += bounty.amount
        if result == 0:
            if not len(Bounty.get_user_bounty_transactions(chat_id, user_id)):
                Bounty.transaction(chat_id, to_user=user_id, amount=INIT_BONUS, description='Init')
                result = INIT_BONUS
        return round(result, 2)

    @classmethod
    def get_fixed_user_bounty(cls, chat_id, user_id):
        result = cls.get_user_bounty(chat_id, user_id)
        return result if result > 0.0 else 0.0

    @classmethod
    def validate_amount(cls, chat_id, user_id, amount):
        from_user_bounty = cls.get_user_bounty(chat_id, user_id)
        delta = cls.get_user_bounty(chat_id, user_id) - amount
        if delta < 0:
            raise NotEnoughBounty(delta, from_user_bounty)
        return True

    @classmethod
    def transaction(cls, chat_id, from_user=None, to_user=None, amount=0, description='transaction', transfer=False):
        # Validate transfer
        if transfer:
            cls.validate_amount(chat_id, from_user, amount)

        # Make transfer
        bounty = Bounty(chat_id=chat_id,
                        from_user=from_user,
                        to_user=to_user,
                        amount=amount,
                        description=description,
                        transfer=transfer)
        bounty.save()

        return bounty


class NotEnoughBounty(ValidationError):
    def __init__(self, need, found):
        self.need = need
        self.found = found
        self.message = 'Need {} bounty but found {}'.format(need, found)

    def __str__(self):
        return self.message
