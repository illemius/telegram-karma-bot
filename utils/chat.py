import sys
import time

from TranslateLib import translate as _

import telebot
from config import LOGGING_CHAT, ROOT_UID
from meta import bot, root_user
from models.dialogs import Dialogs
from utils.chat_logger import get_chat_logger
from utils.logging import CrashReport

chat_log = get_chat_logger(LOGGING_CHAT, 'main')


def typing(message, timeout=.5):
    if timeout > .0:
        time.sleep(timeout)
    if type(message) is int:
        chat_id = message
    else:
        chat_id = message.chat.id
    bot.send_chat_action(chat_id, 'typing')


def is_private(message):
    if hasattr(message, 'chat'):
        if message.chat.type == 'private':
            return True
    return False


def check_dialog(message, callback, model):
    def handle_message(msg: telebot.types.Message):
        if msg.text:
            data = msg.text.split(' | ')
            raw_index = data[0].strip()[1:]
            if raw_index.isdigit():
                try:
                    bot.reply_to(msg, 'Выбран диалог: ' + ' | '.join(data[1:]), reply_markup=telebot.types.ReplyKeyboardHide())
                    return callback(msg, -int(raw_index))
                except:
                    crash_message(message)
        return bot.reply_to(message, 'Отменено.', reply_markup=telebot.types.ReplyKeyboardHide())

    if is_private(message):
        assert hasattr(model, 'objects')
        assert hasattr(model, 'chat_id')

        dialogs = []
        if hasattr(model, 'from_user'):
            for obj in model.objects(from_user=message.from_user.id):
                if obj.chat_id not in dialogs:
                    dialogs.append(obj.chat_id)
        if hasattr(model, 'to_user'):
            for obj in model.objects(to_user=message.from_user.id):
                if obj.chat_id not in dialogs:
                    dialogs.append(obj.chat_id)
        if hasattr(model, 'user_id'):
            for obj in model.objects(user_id=message.from_user.id):
                if obj.chat_id not in dialogs:
                    dialogs.append(obj.chat_id)

        markup = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True)
        for chat_id in dialogs:
            try:
                pass
                markup.add(str(chat_id) + ' | ' + bot.get_chat(chat_id).title)
            except:
                pass
        markup.add('Отменить.')
        bot.reply_to(message, 'Выберите диалог', reply_markup=markup)
        return bot.register_next_step_handler(message, handle_message)
    return callback(message, message.chat.id)


def sender_is_admin(chat_id, user_id):
    if user_id == ROOT_UID or user_id in [user.user.id for user in bot.get_chat_administrators(chat_id)]:
        return True
    return False


def get_username_or_name(user):
    if type(user) not in [telebot.types.User, telebot.types.Chat]:
        return '<USER>'
    if user.username is None:
        name = '{} {}'.format(user.first_name or '', user.last_name or '').strip()
        return name if len(name) else user.id
    return '@' + user.username


def get_chat_url(message, to_message=False):
    if to_message:
        url = get_chat_url(message, to_message=False)
        if url:
            return url + '/' + str(message.message_id)
    if message.chat.username:
        return 'https://telegram.me/' + message.chat.username
    return None


def get_chat_url_or_title(message):
    url = get_chat_url(message, True)
    if url:
        return '[{}]({})'.format(message.chat.title, url)
    return message.chat.title


def cant_send_private(message, locale=None):
    bot.reply_to(message, _('can not sent private message', locale=locale))


def crash_message(message):
    c = CrashReport(*sys.exc_info())
    try:
        c.save()
        telebot.logger.error('\n\t'.join(c.formatted_traceback))
        bot.send_message(message.chat.id, 'Во время выполнения команды произошла ошибка :с\n'
                                          'Если не сложно, перешли, пожалуйста это сообщение разработчику '
                                          '{user}\n'
                                          'Report: {id}-{file}'.format(user=get_username_or_name(root_user),
                                                                       id=c.id, file=c.filename))
        chat_log.error('Crash report: <b>{id}</b>\n'
                       'File: <b>{file}</b>\n'
                       'User: {login} ({uid})\n'
                       '<pre>{traceback}</pre>'.format(id=c.id, file=c.filename,
                                                       login=get_username_or_name(message.from_user),
                                                       uid=message.from_user.id,
                                                       traceback='\n'.join(c.formatted_traceback)))
    except:
        cc = CrashReport(*sys.exc_info())
        print('\n'.join(c.formatted_traceback), file=sys.stderr)
        print('\n'.join(cc.formatted_traceback), file=sys.stderr)


def get_dialog_object(index):
    dialogs = Dialogs.objects(index=index)
    if len(dialogs):
        return dialogs[0]
    else:
        return Dialogs(index=index)


class ParseUserData(object):
    def __init__(self, function):
        self.function = function

    def __call__(self, message):
        return self.function(message,
                             user=get_dialog_object(message.from_user.id),
                             chat=get_dialog_object(message.chat.id))


KEY_SPLITTER = '^~'
DATA_SPLITTER = "^^:"


def generate_inline_data(key: str, data: list):
    return '{}{}{}'.format(key, KEY_SPLITTER, DATA_SPLITTER.join(data))


def parse_inline_data(data):
    key = data.split(KEY_SPLITTER)[0]
    data = data[len(key) + len(KEY_SPLITTER):].split(DATA_SPLITTER)
    return key, data
