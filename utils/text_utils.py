def startswith(data, items):
    for item in items:
        if data.startswith(item):
            return True
    return False


def contains(data, items):
    for item in items:
        if item in data:
            return True
    return False


def count(data, items):
    result = 0
    for item in items:
        result += data.count(item)
    return result


def format_message(key, value, description=None, space=0, html=False):
    def generate():
        yield ' ' * space
        yield '<b>{key}</b>: ' if html else '{key}: '
        yield '<i>{value}</i>' if html else '{value}'
        if description is not None:
            yield ' (<pre>{description}</pre>)' if html else '({description})'

    return ''.join(generate()).format(key=key, value=value, description=description)


def get_element(data, index, default=None):
    try:
        return data[index]
    except IndexError:
        return default
